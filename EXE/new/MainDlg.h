// MainDlg.h : interface of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////
#pragma once
#include <map>
#include <iostream>
#include <string>
#include <windows.h>
#include <unordered_map>
#include <sstream>
#include <fstream>
#include <atlbase.h>
using namespace std;
//using FUN1 = void(*)(unordered_map<string, string> &ump, ifstream &ifs);
//extern unordered_map<string, string>up;
//using FUN2=


wstring string2wstring(string str);
string wstring2string(wstring wstr);
class CMainDlg : public SHostWnd
{
public:
	CMainDlg();
	~CMainDlg();
	
	void OnClose();
	void OnMaximize();
	void OnRestore();
	void OnMinimize();

	
	void increase();
	void Delete();
	void change();
	void search();

	

	void OnSize(UINT nType, CSize size);
	BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam);

	bool OnBtIncreaseClick(EventArgs *pEvt);
protected:
	//soui消息
	EVENT_MAP_BEGIN()
		EVENT_NAME_COMMAND(L"btn_close", OnClose)
		EVENT_NAME_COMMAND(L"btn_min", OnMinimize)
		EVENT_NAME_COMMAND(L"btn_max", OnMaximize)
		EVENT_NAME_COMMAND(L"btn_restore", OnRestore)
		EVENT_NAME_COMMAND(L"btn_search", search)
		EVENT_NAME_COMMAND(L"btn_increase",increase)
		EVENT_NAME_COMMAND(L"btn_delete", Delete)
		EVENT_NAME_COMMAND(L"btn_change", change)
	EVENT_MAP_END()
		
	//HostWnd真实窗口消息处理
	BEGIN_MSG_MAP_EX(CMainDlg)
		MSG_WM_INITDIALOG(OnInitDialog)
		MSG_WM_CLOSE(OnClose)
		MSG_WM_SIZE(OnSize)
		CHAIN_MSG_MAP(SHostWnd)
		REFLECT_NOTIFICATIONS_EX()
	END_MSG_MAP()

	void InitListCtrl();
};
