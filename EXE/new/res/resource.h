//stamp:075b7dbfce485bbb
/*<------------------------------------------------------------------------------------------------->*/
/*该文件由uiresbuilder生成，请不要手动修改*/
/*<------------------------------------------------------------------------------------------------->*/
#pragma once
#include <res.mgr/snamedvalue.h>
#define ROBJ_IN_CPP \
namespace SOUI \
{\
    const _R R;\
    const _UIRES UIRES;\
}
namespace SOUI
{
	class _UIRES{
		public:
		class _UIDEF{
			public:
			_UIDEF(){
				XML_INIT = _T("UIDEF:XML_INIT");
			}
			const TCHAR * XML_INIT;
		}UIDEF;
		class _LAYOUT{
			public:
			_LAYOUT(){
				XML_MAINWND = _T("LAYOUT:XML_MAINWND");
			}
			const TCHAR * XML_MAINWND;
		}LAYOUT;
		class _values{
			public:
			_values(){
				string = _T("values:string");
				color = _T("values:color");
				skin = _T("values:skin");
			}
			const TCHAR * string;
			const TCHAR * color;
			const TCHAR * skin;
		}values;
		class _IMG{
			public:
			_IMG(){
			}
		}IMG;
		class _ICON{
			public:
			_ICON(){
				ICON_LOGO = _T("ICON:ICON_LOGO");
			}
			const TCHAR * ICON_LOGO;
		}ICON;
	};
	const SNamedID::NAMEDVALUE namedXmlID[]={
		{L"_name_start",65535},
		{L"btn_change",65544},
		{L"btn_close",65536},
		{L"btn_delete",65543},
		{L"btn_increase",65542},
		{L"btn_max",65537},
		{L"btn_min",65539},
		{L"btn_restore",65538},
		{L"btn_search",65541},
		{L"edit_1",65540},
		{L"edit_2",65546},
		{L"edit_3",65547},
		{L"lc_test",65545}		};
	class _R{
	public:
		class _name{
		public:
		_name(){
			_name_start = namedXmlID[0].strName;
			btn_change = namedXmlID[1].strName;
			btn_close = namedXmlID[2].strName;
			btn_delete = namedXmlID[3].strName;
			btn_increase = namedXmlID[4].strName;
			btn_max = namedXmlID[5].strName;
			btn_min = namedXmlID[6].strName;
			btn_restore = namedXmlID[7].strName;
			btn_search = namedXmlID[8].strName;
			edit_1 = namedXmlID[9].strName;
			edit_2 = namedXmlID[10].strName;
			edit_3 = namedXmlID[11].strName;
			lc_test = namedXmlID[12].strName;
		}
		 const wchar_t * _name_start;
		 const wchar_t * btn_change;
		 const wchar_t * btn_close;
		 const wchar_t * btn_delete;
		 const wchar_t * btn_increase;
		 const wchar_t * btn_max;
		 const wchar_t * btn_min;
		 const wchar_t * btn_restore;
		 const wchar_t * btn_search;
		 const wchar_t * edit_1;
		 const wchar_t * edit_2;
		 const wchar_t * edit_3;
		 const wchar_t * lc_test;
		}name;

		class _id{
		public:
		const static int _name_start	=	65535;
		const static int btn_change	=	65544;
		const static int btn_close	=	65536;
		const static int btn_delete	=	65543;
		const static int btn_increase	=	65542;
		const static int btn_max	=	65537;
		const static int btn_min	=	65539;
		const static int btn_restore	=	65538;
		const static int btn_search	=	65541;
		const static int edit_1	=	65540;
		const static int edit_2	=	65546;
		const static int edit_3	=	65547;
		const static int lc_test	=	65545;
		}id;

		class _string{
		public:
		const static int title	=	0;
		const static int ver	=	1;
		}string;

		class _color{
		public:
		const static int blue	=	0;
		const static int gray	=	1;
		const static int green	=	2;
		const static int red	=	3;
		const static int white	=	4;
		}color;

	};

#ifdef R_IN_CPP
	 extern const _R R;
	 extern const _UIRES UIRES;
#else
	 extern const __declspec(selectany) _R & R = _R();
	 extern const __declspec(selectany) _UIRES & UIRES = _UIRES();
#endif//R_IN_CPP
}
