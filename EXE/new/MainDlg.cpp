// MainDlg.cpp : implementation of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MainDlg.h"	
#include "..\..\ump_dll\ump_dll\ump_dll.h"


CMainDlg::CMainDlg() : SHostWnd(_T("LAYOUT:XML_MAINWND"))
{
	
}

CMainDlg::~CMainDlg()
{
}

BOOL CMainDlg::OnInitDialog(HWND hWnd, LPARAM lParam)
{
	InitListCtrl();
	return 0;
}

bool CMainDlg::OnBtIncreaseClick(EventArgs *pEvt)
{
	return true;
}

//TODO:消息映射

void CMainDlg::OnClose()
{
	SNativeWnd::DestroyWindow();
}

void CMainDlg::OnMaximize()
{
	SendMessage(WM_SYSCOMMAND, SC_MAXIMIZE);
}
void CMainDlg::OnRestore()
{
	SendMessage(WM_SYSCOMMAND, SC_RESTORE);
}
void CMainDlg::OnMinimize()
{
	SendMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}

void CMainDlg::increase(){
	SEdit *edit2 = FindChildByName2<SEdit>(L"edit_2");
	SEdit *edit3 = FindChildByName2<SEdit>(L"edit_3");
	auto res2 = edit2->GetWindowTextW();
	//MessageBoxW(m_hWnd, res, _T("查询结果"), MB_OK);
	auto xx = res2.c_str();
	wstring new_name = xx;
	string name = wstring2string(new_name);

	auto res3 = edit3->GetWindowTextW();
	//MessageBoxW(m_hWnd, res, _T("查询结果"), MB_OK);
	auto cc = res3.c_str();
	wstring new_number = cc;
	string number = wstring2string(new_number);


	fun::init();
	
	int z = fun::insert(name.c_str(), number.c_str());
	if (z==1)
	MessageBoxW(m_hWnd, _T("添加成功"), _T("添加结果"), MB_OK);
	else
	MessageBoxW(m_hWnd, _T("添加失败"), _T("添加结果"), MB_OK);
	InitListCtrl();
	
}
void CMainDlg::Delete(){
	SEdit *edit1 = FindChildByName2<SEdit>(L"edit_1");
	auto res = edit1->GetWindowTextW();
	
	auto xx = res.c_str();
	wstring name1 = xx;
	string name = wstring2string(name1);

	int z = fun::deletet(name.c_str());
	if (z == 1)
		MessageBoxW(m_hWnd, _T("删除成功"), _T("删除结果"), MB_OK);
	else
		MessageBoxW(m_hWnd, _T("删除失败"), _T("删除结果"), MB_OK);
	InitListCtrl();
}
void CMainDlg::change(){
	SEdit *edit2 = FindChildByName2<SEdit>(L"edit_2");
	SEdit *edit3 = FindChildByName2<SEdit>(L"edit_3");
	auto res2 = edit2->GetWindowTextW();
	//MessageBoxW(m_hWnd, res, _T("查询结果"), MB_OK);
	auto xx = res2.c_str();
	wstring new_name = xx;
	string name = wstring2string(new_name);

	auto res3 = edit3->GetWindowTextW();
	//MessageBoxW(m_hWnd, res, _T("查询结果"), MB_OK);
	auto cc = res3.c_str();
	wstring new_number = cc;
	string number = wstring2string(new_number);

	int z = fun::changee(name.c_str(), number.c_str());
	if (z == 1)
		MessageBoxW(m_hWnd, _T("修改成功"), _T("修改结果"), MB_OK);
	else
		MessageBoxW(m_hWnd, _T("修改失败"), _T("修改结果"), MB_OK);
	InitListCtrl();
}
void CMainDlg::search(){
	SEdit *edit1 = FindChildByName2<SEdit>(L"edit_1");
	auto res = edit1->GetWindowTextW();
	//MessageBoxW(m_hWnd, res, _T("查询结果"), MB_OK);
	auto xx = res.c_str();
	wstring name1=xx;
	string name = wstring2string(name1);
	string s = fun::searchn(name.c_str());
	auto result = string2wstring(s);
	MessageBoxW(m_hWnd, result.c_str(), _T("查询结果"), MB_OK);
	
}



void CMainDlg::OnSize(UINT nType, CSize size)
{
	SetMsgHandled(FALSE);	
	SWindow *pBtnMax = FindChildByName(L"btn_max");
	SWindow *pBtnRestore = FindChildByName(L"btn_restore");
	if(!pBtnMax || !pBtnRestore) return;
	
	if (nType == SIZE_MAXIMIZED)
	{
		pBtnRestore->SetVisible(TRUE);
		pBtnMax->SetVisible(FALSE);
	}
	else if (nType == SIZE_RESTORED)
	{
		pBtnRestore->SetVisible(FALSE);
		pBtnMax->SetVisible(TRUE);
	}
}
struct people{
	wstring name;
	wstring number;
};


string wstring2string(wstring wstr)
{
	string result;
	//获取缓冲区大小，并申请空间，缓冲区大小事按字节计算的  
	int len = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), NULL, 0, NULL, NULL);
	char* buffer = new char[len + 1];
	//宽字节编码转换成多字节编码  
	WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), wstr.size(), buffer, len, NULL, NULL);
	buffer[len] = '\0';
	//删除缓冲区并返回值  
	result.append(buffer);
	delete[] buffer;
	return result;
}
wstring string2wstring(string str)
{
	wstring result;
	//获取缓冲区大小，并申请空间，缓冲区大小按字符计算  
	int len = MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), NULL, 0);
	TCHAR* buffer = new TCHAR[len + 1];
	//多字节编码转换成宽字节编码  
	MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), buffer, len);
	buffer[len] = '\0';             //添加字符串结尾  
	//删除缓冲区并返回值  
	result.append(buffer);
	delete[] buffer;
	return result;
}
void CMainDlg::InitListCtrl(){
	SListCtrl *pList = FindChildByName2<SListCtrl>(L"lc_test");
	pList->DeleteAllItems();

	
	if (pList){
		SWindow *pHeader = pList->GetWindow(GSW_FIRSTCHILD);
		singlemap* re = singlemap::getsmp();
		fun::init();
		for (auto i : re->m)
		{
			people* pst = new people;
			pst->name = string2wstring(i.first);
			pst->number = string2wstring(i.second);
			int item = pList->InsertItem(0, pst->name.c_str());
			pList->SetItemData(item, (LPARAM)pst);
			pList->SetSubItemText(item, 1, pst->number.c_str());
		}
		SOUI::SWindow* pIncrease = FindChildByName(L"btn_increase");
		if (pIncrease)
		{
			pIncrease->GetEventSet()->subscribeEvent(EventLButtonUp::EventID, Subscriber(&CMainDlg::OnBtIncreaseClick, this));
		}
	}
}

