#pragma once

#ifdef _WIN32
#ifdef DLL_API_EXPORTS
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __declspec(dllimport)
#endif
#else
#define DLL_API
#endif
#include <iostream>
#include <fstream>
#include <utility>
#include <unordered_map>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

class DLL_API singlemap
{
private:
	singlemap();
	static singlemap* smp;
public:
	unordered_map<string, string>m;
	static singlemap* getsmp();

};
class DLL_API fun{
public:
	static void init();
	static const char* searchn(const char* name);
	static int insert(const char* name, const  char* number);
	static int deletet(const char* name);
	static int changee(const char* name, const char* number);
	static void update();
};

//void update(unordered_map<string, string> &ump);